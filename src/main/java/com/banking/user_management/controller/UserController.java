package com.banking.user_management.controller;

import com.banking.user_management.dto.RevokeUserPermission;
import com.banking.user_management.dto.UserUpdateRequest;
import com.banking.user_management.dto.request.*;
import com.banking.user_management.dto.response.ApiResponseDto;
import com.banking.user_management.service.UsersService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/users")
@AllArgsConstructor
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    private final UsersService usersService;


    @PostMapping("/login")
    public ResponseEntity<?> authenticationUser(@Valid @RequestBody LoginRequest request){
        log.info("Authentication users, Request : {}",request);
        return new ResponseEntity<>(usersService.login(request), HttpStatus.OK);
    }

    @PostMapping("/get-access-token")
    public ResponseEntity<?> getAccessTokenByRefreshToken(@RequestBody @Valid GetAccessTokenRequest request,
                                                          @AuthenticationPrincipal UserDetails userDetails){
        return new ResponseEntity<>(usersService.getAccessToken(request, userDetails), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<ApiResponseDto> createUsers(@RequestBody @Valid CreateUserRequest request) throws Exception {
        log.info("Create-User API called for username: {}", request.getUsername());
        return new ResponseEntity<>(usersService.createUser(request), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/enable")
    public ResponseEntity<ApiResponseDto> enableUser(@RequestBody @Valid EnableUserRequest request) {
        log.info("Enable-User API called for username: {}", request.getUsername());
        return new ResponseEntity<>(usersService.enabledUser(request), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('DISABLE_USER')")
    @PostMapping("/disable")
    public ResponseEntity<ApiResponseDto> disableUser(@RequestBody @Valid DisableUser request) {
        log.info("Disable-User API called for username: {}", request.getUsername());
        return new ResponseEntity<>(usersService.disableUser(request), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('DELETE_USER')")
    @PostMapping("/delete")
    public ResponseEntity<ApiResponseDto> deleteUser(@RequestBody @Valid DeleteUserRequest request){
        log.info("Delete-User API called for username: {}", request.getUsername());
        return new ResponseEntity<>(usersService.deleteUser(request), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('UPDATE_USER')")
    @PutMapping("/{id}/update")
    public ResponseEntity<ApiResponseDto> updateUser(@RequestBody @Valid UserUpdateRequest request, @PathVariable("id") Integer userId){
        log.info("Update-User API called for username: {}", request.getUsername());
        return new ResponseEntity<>(usersService.updateUser(request, userId), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CHANGE_PASSWORD')")
    @PostMapping("/{id}/reset-password")
    public ResponseEntity<ApiResponseDto> changePassword(@RequestBody @Valid ResetPassword request, @PathVariable("id") Integer id){
        log.info("Change-Password API called for userId: {}", id);
        return new ResponseEntity<>(usersService.resetPassword(request, id), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CHANGE_PASSWORD')")
    @PostMapping("/change-new-password")
    public ResponseEntity<ApiResponseDto> changeNewPassword(@RequestBody @Valid ChangeNewPasswordRequest request, @AuthenticationPrincipal UserDetails userDetails){
       log.info("Change-New-Password API called...");
       return new ResponseEntity<>(usersService.changeNewPassword(request, userDetails), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ASSIGN_USER_ROLE')")
    @PostMapping("/assign-roles")
    public ResponseEntity<ApiResponseDto> assignRoleToUser(@RequestBody @Valid AssignRoleRequest request){
        log.info("Assign-Role API called...");
        return new ResponseEntity<>(usersService.assignRoleToUser(request), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('LIST_USER')")
    @GetMapping("/list")
    public ResponseEntity<?> listUser(ListUserRequest request){
        return new ResponseEntity<>(usersService.listUser(request), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('VIEW_PROFILE')")
    @GetMapping("/{id}/view-profile")
    public ResponseEntity<?> viewProfile(@PathVariable("id") Integer id){
        return new ResponseEntity<>(usersService.viewProfile(id), HttpStatus.OK);
    }

    @PostMapping("/revoke-user-permission")
    public ResponseEntity<?> revokePermissionFromUser(@RequestBody @Valid RevokeUserPermission revokeUserPermission){
        return new ResponseEntity<>(usersService.revokeUserPermission(revokeUserPermission), HttpStatus.OK);
    }
}
