package com.banking.user_management.controller;

import com.banking.user_management.dto.pagination.PaginationRequest;
import com.banking.user_management.dto.request.CreateRoleRequest;
import com.banking.user_management.dto.request.RevokeRolePermissionRequest;
import com.banking.user_management.dto.request.UpdateRolePermissionRequest;
import com.banking.user_management.service.RoleService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/roles")
public class RoleController {

    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PreAuthorize("hasRole('CREATE_ROLE')")
    @PostMapping("/create")
    public ResponseEntity<?> createRole(@RequestBody @Valid CreateRoleRequest request){
        return new ResponseEntity<>(roleService.createRole(request), HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('CREATE_ROLE')")
    @PostMapping("/revoke-permissions")
    public ResponseEntity<?> revokeRolePermission(@RequestBody @Valid RevokeRolePermissionRequest request){
        return new ResponseEntity<>(roleService.revokeRolePermission(request), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('UPDATE_ROLE')")
    @PatchMapping("/update")
    public ResponseEntity<?> updateRole(@RequestBody @Valid UpdateRolePermissionRequest request){
        return new ResponseEntity<>(roleService.updateRolePermission(request), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('DELETE_ROLE')")
    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> deleteRole(@PathVariable("id") @NotNull Integer roleId){
        return new ResponseEntity<>(roleService.deleteRole(roleId), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('LIST_ROLE')")
    @GetMapping("/list")
    public ResponseEntity<?> listRole(PaginationRequest request){
        return new ResponseEntity<>(roleService.listRole(request), HttpStatus.OK);
    }
}
