package com.banking.user_management.controller;

import com.banking.user_management.dto.pagination.PaginationRequest;
import com.banking.user_management.service.PermissionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/permission")
public class PermissionController {

    private final PermissionService permissionService;

    public PermissionController(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> listPermission(PaginationRequest request){
        return new ResponseEntity<>(permissionService.getPermissionsWithPagination(request), HttpStatus.OK);
    }

}
