package com.banking.user_management.dto.pagination;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
public class PaginationResponse<T> {

    private int currentPage;
    private int totalPages;
    private long totalElements;
    private int pageSize;
    private List<T> content;

}