package com.banking.user_management.dto.pagination;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PaginationRequest {
    private Integer page = 0;
    private Integer size = 10;
}
