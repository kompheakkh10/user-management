package com.banking.user_management.dto;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.banking.user_management.Codes.Code;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class AppBaseResponse {

    private final double code;
    private final String message;

    public AppBaseResponse(Code code, MessageSource messageSource, Object... args) {
        this(code.code(), messageSource.getMessage(code.messageKey(), args, LocaleContextHolder.getLocale()));
    }

    public AppBaseResponse(Code code, String message) {
        this(code.code(), message);
    }
}
