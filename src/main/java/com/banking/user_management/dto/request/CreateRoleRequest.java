package com.banking.user_management.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CreateRoleRequest {
    @NotBlank(message = "{error.require.role-name}")
    private String roleName;
    private String description;
    @NotEmpty(message = "{error.require.role-name}")
    private List<String> listPermissionName;
}
