package com.banking.user_management.dto.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class RevokeRolePermissionRequest {

    @NotNull(message = "{error.require.role-id}")
    private Integer roleId;
    @NotEmpty(message = "{error.require.role-name}")
    private List<String> listOfPermissions;

}
