package com.banking.user_management.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UpdateRolePermissionRequest {

    @NotNull(message = "{error.require.role-id}")
    private int roleId;
    private String roleName;
    private String description;

}
