package com.banking.user_management.dto.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EmailRequest {
    private String username;
    private String sendTo;
    private String randomPassword;
}
