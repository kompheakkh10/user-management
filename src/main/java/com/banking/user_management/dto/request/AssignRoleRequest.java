package com.banking.user_management.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class AssignRoleRequest {
    @NotNull(message = "{error.require.user-id}")
    private int userId;

    @NotNull(message = "{error.require.role}")
    private List<Integer> listRoleId;
}
