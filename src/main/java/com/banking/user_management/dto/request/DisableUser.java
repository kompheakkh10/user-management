package com.banking.user_management.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DisableUser {

    @NotBlank(message = "{error.require.name.field}")
    private String username;

}
