package com.banking.user_management.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChangeNewPasswordRequest {
    @NotBlank(message = "{error.require.password-key}")
    private String passwordKey;
    @NotBlank(message = "{error.require.new-password}")
    private String newPassword;
    @NotBlank(message = "{error.require.new-password}")
    private String confirmPassword;
}
