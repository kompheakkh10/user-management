package com.banking.user_management.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GetAccessTokenRequest {
    @NotBlank(message = "{error.require.refresh-token}")
    private String refreshToken;
}
