package com.banking.user_management.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class LoginRequest {
    @NotBlank(message = "{error.require.name.field}")
    private String username;

    @NotBlank(message = "{error.require.password.field}")
    private String password;
}
