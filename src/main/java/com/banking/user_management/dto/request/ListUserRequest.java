package com.banking.user_management.dto.request;

import com.banking.user_management.dto.pagination.PaginationRequest;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ListUserRequest extends PaginationRequest {

    private String username = "";

}
