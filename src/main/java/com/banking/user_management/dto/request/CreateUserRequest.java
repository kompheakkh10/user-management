package com.banking.user_management.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreateUserRequest {

    @NotBlank(message = "{error.require.name.field}")
    private String username;

    @NotBlank(message = "{error.require.new-password}")
    private String password;

    @NotEmpty(message = "{error.require.email}")
    @Email(message = "{error.incorrect-format-email}")
    private String email;

    // set to default value
    @JsonIgnore
    private boolean enabled;
    @JsonIgnore
    private boolean deleted;
}
