package com.banking.user_management.dto;

import java.util.List;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = false)
public class ModelStateResponse extends AppBaseResponse {

    private final List<ErrorState> errors;

    public ModelStateResponse(double code, String message, List<ErrorState> errors) {
        super(code, message);
        this.errors = errors;
    }
}
