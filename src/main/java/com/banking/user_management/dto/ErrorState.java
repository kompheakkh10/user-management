package com.banking.user_management.dto;

import java.util.List;

import lombok.Getter;

@Getter
public class ErrorState {

    private final String name;
    private final List<String> messages;

    public ErrorState(String name, String... messages) {
        this.name = name;
        this.messages = List.of(messages);
    }

    public ErrorState(String name, List<String> messages) {
        this.name = name;
        this.messages = messages;
    }
}
