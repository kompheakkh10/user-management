package com.banking.user_management.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ApiDetailsResponseDto <T>{
    private String message;
    private Integer statusCode;
    private T data;
}
