package com.banking.user_management.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleResponse {
    private String roleName;
    private String description;
}
