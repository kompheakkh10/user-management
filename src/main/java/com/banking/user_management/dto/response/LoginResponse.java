package com.banking.user_management.dto.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginResponse {
    private String username;
    private String accessToken;
    private String refreshToken;
}
