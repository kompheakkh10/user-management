package com.banking.user_management.dto.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RoleFilterResponse {

    private Integer id;
    private String roleName;
    private String description;

}
