package com.banking.user_management.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class UserProfileResponse {

    private Integer id;
    private String username;
    private String email;
    private Boolean isEnabled;
    private String description;
    private List<RoleResponse> roles;

}
