package com.banking.user_management.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class ApiResponseDto {
    private String message;
    private Integer status;

    public static ApiResponseDto create(){
        ApiResponseDto response = new ApiResponseDto();
        response.setMessage("create");
        response.setStatus(HttpStatus.CREATED.value());

        return response;
    }

    public static ApiResponseDto success(){
        ApiResponseDto response = new ApiResponseDto();
        response.setMessage("success");
        response.setStatus(HttpStatus.OK.value());

        return response;
    }
}
