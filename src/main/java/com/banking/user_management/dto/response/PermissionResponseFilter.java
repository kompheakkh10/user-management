package com.banking.user_management.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
public class PermissionResponseFilter {
    private String name;
    private LocalDateTime createdDate;
    private LocalDateTime updatedDate;
    private String remarks;
}
