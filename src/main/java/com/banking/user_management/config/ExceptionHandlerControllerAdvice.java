package com.banking.user_management.config;

import com.banking.user_management.Codes;
import com.banking.user_management.dto.AppBaseResponse;
import com.banking.user_management.dto.ErrorState;
import com.banking.user_management.dto.ModelStateResponse;
import com.banking.user_management.exception.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

@ControllerAdvice
@RequiredArgsConstructor
public class ExceptionHandlerControllerAdvice {

    private final MessageSource messageSource;

    @ExceptionHandler(CustomUnauthorizedException.class)
    public ResponseEntity<CustomErrorResponseException<?>> handleUnauthorized(CustomUnauthorizedException exception) {
        CustomErrorResponseException<Object> customErrorResponseException = CustomErrorResponseException.builder()
                .timeStamp(new Date())
                .status(HttpStatus.UNAUTHORIZED.value())
                .message(exception.getMessage())
                .build();

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(customErrorResponseException);
    }

    @ExceptionHandler(CustomForbiddenException.class)
    public ResponseEntity<CustomErrorResponseException<?>> handleForbidden(CustomForbiddenException exception) {
        CustomErrorResponseException<Object> errorResponseException = CustomErrorResponseException.builder()
                .timeStamp(new Date())
                .status(HttpStatus.FORBIDDEN.value())
                .message(exception.getMessage())
                .build();

        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(errorResponseException);
    }

    @ExceptionHandler(CustomNotFoundException.class)
    public ResponseEntity<CustomErrorResponseException<?>> handleNotFound(CustomNotFoundException exception) {
        CustomErrorResponseException<Object> errorResponseException = CustomErrorResponseException.builder()
                .timeStamp(new Date())
                .status(HttpStatus.NOT_FOUND.value())
                .message(exception.getMessage())
                .build();

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponseException);
    }

    @ExceptionHandler(CustomInternalServerErrorException.class)
    public ResponseEntity<CustomErrorResponseException<?>> handleInternalServerError(
            CustomInternalServerErrorException exception) {
        CustomErrorResponseException<Object> errorResponseException = CustomErrorResponseException.builder()
                .timeStamp(new Date())
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(exception.getMessage())
                .build();

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponseException);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handle(MethodArgumentNotValidException ex) {
        final Function<Entry<String, List<FieldError>>, ErrorState> getErrorState = e -> new ErrorState(
                e.getKey(),
                e.getValue().stream().map(FieldError::getDefaultMessage).collect(Collectors.toList()));
        final var fields = ex.getBindingResult()
                .getAllErrors()
                .stream()
                .map(FieldError.class::cast)
                .collect(Collectors.groupingBy(FieldError::getField));
        final var errors = fields
                .entrySet()
                .stream()
                .map(getErrorState)
                .collect(Collectors.toList());
        final var message = messageSource.getMessage(
                Codes.BAD_REQUEST.messageKey(),
                null,
                Codes.BAD_REQUEST.messageKey(),
                LocaleContextHolder.getLocale());

        final var response = new ModelStateResponse(
                Codes.BAD_REQUEST.code(),
                message,
                errors);
        return ResponseEntity
                .badRequest()
                .body(response);
    }

    @ExceptionHandler(CustomErrorDatabase.class)
    public ResponseEntity<CustomErrorResponseException<?>> handleErrorDataBase(CustomErrorDatabase exception) {
        CustomErrorResponseException<Object> errorResponseException = CustomErrorResponseException.builder()
                .timeStamp(new Date())
                .status(HttpStatus.BAD_REQUEST.value())
                .message(exception.getMessage())
                .build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseException);
    }

    @ExceptionHandler(AppException.class)
    public ResponseEntity<AppBaseResponse> handle(AppException exception) {
        final var response = new AppBaseResponse(
                exception.getCode().code(),
                exception.getLocalizedMessage());
        return ResponseEntity
                .status(exception.getStatus())
                .body(response);
    }

}
