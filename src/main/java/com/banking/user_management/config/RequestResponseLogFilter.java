package com.banking.user_management.config;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import java.io.IOException;import java.util.UUID;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class RequestResponseLogFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("Init filter");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {        // Generate UUID for overall use
        MDC.put("REQUEST_ID", UUID.randomUUID().toString().replace("-", ""));
        // Log incoming request
        logRequest((HttpServletRequest) request);
        chain.doFilter(request, response);
        // Log outgoing response
        logResponse((HttpServletResponse) response);
    }

    @Override
    public void destroy() {
        log.info("RequestResponseLogFilter destroyed");
    }

    private void logRequest(HttpServletRequest request) {
        log.info("request: url={}, method={}, remote address={}", request.getRequestURI(), request.getMethod(), request.getRemoteAddr());
    }

    private void logResponse(HttpServletResponse response){
        log.info("response: status={}", response.getStatus());
    }
}
