package com.banking.user_management.config;

import com.banking.user_management.entity.Users;
import com.banking.user_management.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
@Slf4j
public class UserDetailsServiceCustom implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailsServiceCustom(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<Users> userOptional = userRepository.findByUsername(username);

        if (userOptional.isEmpty()){
            log.error("User not found with username!: {}", username);
            throw new UsernameNotFoundException("User not found!");
        }

        return new User(userOptional.get().getUsername(),
                userOptional.get().getPassword(),
                getAuthority(userOptional.get())
        );
    }

    private Set<SimpleGrantedAuthority> getAuthority(Users user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();

        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleName()));
            role.getPermissions().forEach(permission ->
                    authorities.add(new SimpleGrantedAuthority("ROLE_"+permission.getName()))
            );
        });
        return authorities;
    }
}
