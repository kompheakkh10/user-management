package com.banking.user_management.config;

import com.banking.user_management.Codes;
import com.banking.user_management.entity.UserAuthentication;
import com.banking.user_management.entity.Users;
import com.banking.user_management.exception.CustomBadRequestException;
import com.banking.user_management.exception.CustomNotFoundException;
import com.banking.user_management.repository.UserAuthenticationRepository;
import com.banking.user_management.repository.UserRepository;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class JwtRequestFilter extends OncePerRequestFilter {
    private final JwtTokenProvider jwtTokenUtil;
    private final UserDetailsService userDetailService;
    private final UserAuthenticationRepository userAuthenticationRepository;
    private final UserRepository userRepository;
    private final MessageSource messageSource;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {

            String header = request.getHeader("Authorization");
            String token = null;
            String username = null;

            if (header != null && header.startsWith("Bearer ")) {
                token = header.replace("Bearer ", "");
                try {
                    username = jwtTokenUtil.getUsernameFromToken(token);
                } catch (IllegalArgumentException e) {
                    log.error("An error occurred while fetching Username from Token", e);
                } catch (ExpiredJwtException e) {
                    log.warn("The token has expired", e);
                } catch (Exception e) {
                    log.error("Authentication Failed. {}", e.getMessage());
                }
            } else {
                log.warn("Couldn't find bearer string, header will be ignored");
            }

            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

                Users user = userRepository.findByUsername(username).orElseThrow(
                        () -> new CustomNotFoundException("User not found!")
                );

                Optional<UserAuthentication> userAuthenticationOptional = userAuthenticationRepository.findByUserId(user.getId());
                UserAuthentication userAuthentication = null;

                if (userAuthenticationOptional.isPresent()) {
                    userAuthentication = userAuthenticationOptional.get();
                } else {
                    log.error("User not found with username: {}", username);
                    throw new CustomNotFoundException("User not found with username");
                }

                if (!Objects.equals(token, userAuthentication.getAccessToken())) {
                    log.error("Token is invalid!");
                    throw new CustomBadRequestException(Codes.BAD_REQUEST, messageSource);
                }


                UserDetails userDetails = userDetailService.loadUserByUsername(username);

                if (Boolean.TRUE.equals(jwtTokenUtil.validateToken(token, userDetails))) {
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                            userDetails, token, userDetails.getAuthorities()
                    );
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                } else {
                    log.warn("Token validation failed");
                }
            }

        } catch (Exception e) {
            log.error("Cannot set user Authentication: {}", e.getMessage());
        }

        filterChain.doFilter(request, response);
    }
}
