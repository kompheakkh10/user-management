package com.banking.user_management.repository;

import com.banking.user_management.entity.RolePermission;
import com.banking.user_management.entity.id.RolesPermissionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolePermissionRepository extends JpaRepository<RolePermission, RolesPermissionId> {

    List<RolePermission> findAllByRoleIdAndPermissionNameIn(Integer roleId, List<String> permissionName);
    void deleteAllByRoleId(Integer roleId);

}
