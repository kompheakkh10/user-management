package com.banking.user_management.repository;

import com.banking.user_management.entity.UserAuthentication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserAuthenticationRepository extends JpaRepository<UserAuthentication, Integer> {

    Optional<UserAuthentication> findByUserId(Integer userId);


}
