package com.banking.user_management.repository;

import com.banking.user_management.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, String>, JpaSpecificationExecutor<Permission> {

    @Query("SELECT p FROM Permission p WHERE p.name IN (:names)")
    List<Permission> findAllByName(@Param("names") List<String> names);

}
