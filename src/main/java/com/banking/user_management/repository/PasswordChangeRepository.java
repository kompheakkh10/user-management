package com.banking.user_management.repository;

import com.banking.user_management.entity.PasswordChange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordChangeRepository extends JpaRepository<PasswordChange, Integer> {

    PasswordChange findByUserId(Integer userId);
    PasswordChange findByPasswordKey(String passwordKey);

}
