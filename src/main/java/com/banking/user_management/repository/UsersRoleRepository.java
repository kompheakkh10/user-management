package com.banking.user_management.repository;

import com.banking.user_management.entity.UsersRoles;
import com.banking.user_management.entity.id.UsersRolesId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRoleRepository extends JpaRepository<UsersRoles, UsersRolesId> {
}

