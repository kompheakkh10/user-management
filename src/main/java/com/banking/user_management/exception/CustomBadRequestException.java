package com.banking.user_management.exception;

import com.banking.user_management.Codes;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

public class CustomBadRequestException extends AppException{

    public CustomBadRequestException(
            Codes.Code code,
            MessageSource messageSource,
            Object... args) {
        super(HttpStatus.BAD_REQUEST, code, messageSource, args);
    }

    public CustomBadRequestException(
            Codes.Code code,
            MessageSource messageSource,
            Throwable cause,
            @Nullable Object... args) {
        super(HttpStatus.BAD_REQUEST, code, messageSource, cause, args);
    }
}
