package com.banking.user_management.exception;

public class CustomErrorDatabase extends RuntimeException{
    public CustomErrorDatabase(String message){
        super(message);
    }
}
