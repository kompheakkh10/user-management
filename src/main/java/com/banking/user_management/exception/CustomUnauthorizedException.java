package com.banking.user_management.exception;

public class CustomUnauthorizedException extends RuntimeException{
    public CustomUnauthorizedException(){}

    public CustomUnauthorizedException(String message){
        super(message);
    }
}
