package com.banking.user_management.exception;

public class CustomForbiddenException extends RuntimeException{
    public CustomForbiddenException(){
    }

    public CustomForbiddenException(String message){
        super(message);
    }
}
