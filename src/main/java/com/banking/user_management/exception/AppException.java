package com.banking.user_management.exception;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

import com.banking.user_management.Codes.Code;

import lombok.Getter;

public class AppException extends RuntimeException {

    private final transient MessageSource messageSource;
    private final transient Object[] args;

    @Getter
    private final Code code;

    @Getter
    private final HttpStatus status;

    AppException(
            HttpStatus status,
            Code code,
            MessageSource messageSource,
            @Nullable Object... args) {
        super();
        this.status = status;
        this.code = code;
        this.messageSource = messageSource;
        this.args = args;
    }

 AppException(
            HttpStatus status,
            Code code,
            MessageSource messageSource,
            Throwable cause,
            @Nullable Object... args) {
        super(cause);
        this.status = status;
        this.code = code;
        this.messageSource = messageSource;
        this.args = args;
    }

    @Override
    public String getLocalizedMessage() {
        return messageSource.getMessage(
                code.messageKey(),
                args,
                LocaleContextHolder.getLocale());
    }

    @Override
    public String getMessage() {
        return messageSource.getMessage(
                code.messageKey(),
                args,
                Locale.getDefault());
    }

}
