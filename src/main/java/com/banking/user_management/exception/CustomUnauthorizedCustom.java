package com.banking.user_management.exception;

import com.banking.user_management.Codes;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

public class CustomUnauthorizedCustom extends AppException{

    public CustomUnauthorizedCustom(
            Codes.Code code,
            MessageSource messageSource,
            @Nullable Object... args) {
        super(HttpStatus.UNAUTHORIZED, code, messageSource, args);
    }

    public CustomUnauthorizedCustom(
            Codes.Code code,
            MessageSource messageSource,
            Throwable cause,
            @Nullable Object... args) {
        super(HttpStatus.UNAUTHORIZED, code, messageSource, cause, args);
    }

}
