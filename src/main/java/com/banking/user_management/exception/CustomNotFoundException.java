package com.banking.user_management.exception;

public class CustomNotFoundException extends RuntimeException{
    public CustomNotFoundException(){}

    public CustomNotFoundException(String message){
        super(message);
    }
}
