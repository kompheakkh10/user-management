package com.banking.user_management.exception;

public class UnauthorizedExceptionCustom extends RuntimeException{

    public UnauthorizedExceptionCustom(String message){
        super(message);
    }

    public UnauthorizedExceptionCustom(Throwable cause){
        super(cause);
    }

}
