package com.banking.user_management.exception;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

import com.banking.user_management.Codes.Code;

public class NotFoundException extends AppException {

    public NotFoundException(
            Code code,
            MessageSource messageSource,
            @Nullable Object... args) {
        super(HttpStatus.NOT_FOUND, code, messageSource, args);
    }

    public NotFoundException(
            Code code,
            MessageSource messageSource,
            Throwable cause,
            @Nullable Object... args) {
        super(HttpStatus.NOT_FOUND, code, messageSource, cause, args);
    }

}
