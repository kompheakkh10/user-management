package com.banking.user_management.exception;

public class CustomInternalServerErrorException extends RuntimeException{
    public CustomInternalServerErrorException(){}

    public CustomInternalServerErrorException(String message){
        super(message);
    }
}
