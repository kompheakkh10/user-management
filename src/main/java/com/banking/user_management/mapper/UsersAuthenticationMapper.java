package com.banking.user_management.mapper;

import com.banking.user_management.entity.UserAuthentication;
import com.banking.user_management.entity.Users;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UsersAuthenticationMapper {

    UsersAuthenticationMapper MAPPER = Mappers.getMapper(UsersAuthenticationMapper.class);

    @Mapping(target = "accessToken" , source = "accessToken")
    @Mapping(target = "attemptFail", source = "loginFailAttempt")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "id", ignore = true)
    UserAuthentication mapToUserAuthentication(String refreshToken, String accessToken, Users user, Integer loginFailAttempt);

}
