package com.banking.user_management.mapper;

import com.banking.user_management.dto.UserUpdateRequest;
import com.banking.user_management.dto.request.CreateRoleRequest;
import com.banking.user_management.dto.request.CreateUserRequest;
import com.banking.user_management.dto.request.EmailRequest;
import com.banking.user_management.dto.request.UpdateRolePermissionRequest;
import com.banking.user_management.dto.response.PermissionResponseFilter;
import com.banking.user_management.dto.response.RoleFilterResponse;
import com.banking.user_management.dto.response.UserProfileResponse;
import com.banking.user_management.dto.response.UserResponseFilter;
import com.banking.user_management.entity.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Mapper
public interface MapperUtils {

    MapperUtils MAPPER = Mappers.getMapper(MapperUtils.class);


    @Mapping(target = "enabled", defaultValue = "true")
    @Mapping(target = "deleted", defaultValue = "false")
    Users userRequestToUsersEntity(CreateUserRequest request);

    void userUpdateRequestToUserEntity(@MappingTarget Users user, UserUpdateRequest request);

    @Mapping(target = "username", source = "user.username")
    @Mapping(target = "randomPassword", source = "randomPassword")
    @Mapping(target = "sendTo", source = "user.email")
    EmailRequest mapToEmailRequest(Users user, String randomPassword);

    @Mapping(target = "user" , source = "user")
    @Mapping(target = "id", ignore = true)
    PasswordChange mapToPasswordChange(Users user, String passwordKey, Timestamp expiredDate);

    @Mapping(target = "user" , source = "user")
    @Mapping(target = "id", ignore = true)
    void mapToPasswordChangeExist(@MappingTarget PasswordChange passwordChange, Users user, String passwordKey, Timestamp expiredDate);

    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "roleId", source = "role.id")
    UsersRoles mapToRolePermission(Users user, Role role);

    Role mapToRole(CreateRoleRequest request);

    @Mapping(target = "roleId", source = "role.id")
    RolePermission mapToRolePermission(Role role, String permissionName);

    default List<RolePermission> mapToRolePermissions(Role role, List<String> permissionNames) {
        return permissionNames.stream()
                .map(permissionName -> mapToRolePermission(role, permissionName))
                .collect(Collectors.toList());
    }

    PermissionResponseFilter mapToResponseFilter(Permission permission);

    void mapToRole(@MappingTarget Role role , UpdateRolePermissionRequest request);

    RoleFilterResponse mapToRoleFilterResponse(Role role);

    UserResponseFilter mapToUsersResponseFilter(Users user);

    UserProfileResponse mapToUserProfileResponse(Users user);
}
