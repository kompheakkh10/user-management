package com.banking.user_management.mapper;

import com.banking.user_management.dto.response.ApiDetailsResponseDto;
import com.banking.user_management.dto.response.LoginResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ResponseMapper {

    ResponseMapper MAPPER = Mappers.getMapper(ResponseMapper.class);

    @Mapping(target = "data.accessToken", source = "accessToken")
    @Mapping(target = "data.username", source = "username")
    @Mapping(target = "data.refreshToken", source = "refreshToken")
    @Mapping(target = "message", constant = "success")
    @Mapping(target = "statusCode", constant = "200")
    ApiDetailsResponseDto<LoginResponse> mapToLoginResponses(String accessToken, String username, String refreshToken);

}
