package com.banking.user_management.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Entity
@Table(name = "PASSWORD_CHANGE")
@Setter
@Getter
public class PasswordChange {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Users user;

    @Column(name = "password_key")
    private String passwordKey;
    @Column(name = "expired_date")
    private Timestamp expiredDate;
}
