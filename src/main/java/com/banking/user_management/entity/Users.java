package com.banking.user_management.entity;

import jakarta.persistence.*;
import jdk.jfr.DataAmount;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;


@Entity
@Table(name = "USERS")
@Data
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column()
    private String email;

    @Setter
    @Column(name = "is_enabled", nullable = false)
    private boolean isEnabled;

    @Column()
    private String description;

    @Column(name = "is_deleted", nullable = false)
    private boolean isDeleted;

    @Column(name = "login_attempt")
    private Integer loginAttempt;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "USERS_ROLES",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;

}
