package com.banking.user_management.entity;

import com.banking.user_management.entity.id.UsersRolesId;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "USERS_ROLES")
@IdClass(UsersRolesId.class)
public class UsersRoles {
    @Id
    @Column(name = "user_id")
    private Integer userId;

    @Id
    @Column(name = "role_id")
    private Integer roleId;
}
