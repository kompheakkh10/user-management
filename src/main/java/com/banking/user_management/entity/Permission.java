package com.banking.user_management.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "PERMISSIONS")
@Setter
@Getter
public class Permission{

    @Id
    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false, name = "created_date")
    private LocalDateTime createdDate;

    @Column(nullable = false, name = "updated_date")
    private LocalDateTime updatedDate;

    @Column()
    private String remarks;

    @ManyToMany(mappedBy = "permissions")
    private Set<Role> roles;

}
