package com.banking.user_management.entity.id;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Setter
@Getter
public class RolesPermissionId implements Serializable {

    private Integer roleId;
    private String permissionName;

    // Default constructor
    public RolesPermissionId() {}

    // Parameterized constructor
    public RolesPermissionId(Integer roleId, String permissionName) {
        this.roleId = roleId;
        this.permissionName = permissionName;
    }

    // Override equals method
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RolesPermissionId that = (RolesPermissionId) o;
        return Objects.equals(roleId, that.roleId) &&
                Objects.equals(permissionName, that.permissionName);
    }

    // Override hashCode method
    @Override
    public int hashCode() {
        return Objects.hash(roleId, permissionName);
    }

}
