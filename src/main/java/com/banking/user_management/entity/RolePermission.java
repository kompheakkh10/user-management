package com.banking.user_management.entity;

import com.banking.user_management.entity.id.RolesPermissionId;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "ROLES_PERMISSION")
@Setter
@Getter
@IdClass(RolesPermissionId.class)
public class RolePermission {

    @Id
    @Column(name = "role_id", nullable = false)
    private Integer roleId;
    @Column(name = "permission_name", nullable = false)
    private String permissionName;

}
