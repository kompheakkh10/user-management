package com.banking.user_management;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

import java.io.Serializable;

public final class Codes {

    private Codes() {

    }

    public record Code(double code, String messageKey) implements Serializable {
    }

    public static final Code OK = new Code(
            200,
            "codes.ok");

    public static final Code BAD_REQUEST = new Code(
            400,
            "codes.bad-request");
    /* Bad Request Code */

    /* Not Found Code */
    public static final Code NOT_FOUND_USER_BY_USERNAME = new Code(
            404.1,
            "codes.not-found.user-by-username");

    public static final Code BAD_REQUEST_TOKEN_IS_INVALID = new Code(
            400,
            "codes.bad-request");

    public static final Code BAD_REQUEST_USER_ALREADY_EXIST = new Code(
            400,
            "codes.user.already.register"
    );

    public static final Code BAD_REQUEST_USER_LOCKED = new Code(
            HttpStatus.UNAUTHORIZED.value(),
            "codes.user.locked"
    );

    public static final Code BAD_REQUEST_INTERNAL_SERVER_ERROR = new Code(
            400,
            "codes.internal.server.error"
    );

    public static final Code BAD_REQUEST_INCORRECT_PASSWORD = new Code(
            400,
            "codes.incorrect.password"
    );

    public static final Code BAD_REQUEST_PASSWORD_KEY_NOT_FOUND = new Code(
            400,
            "codes.password-key.not.found"
    );

    public static final Code BAD_REQUEST_PASSWORD_KEY_NOT_MATCH = new Code(
            400,
            "codes.password-key.not.match"
    );

    public static final Code BAD_REQUEST_ROLE_NOT_FOUND = new Code(
            400,
            "codes.role.not.found"
    );

    public static final Code RESOURCE_NOT_FOUND = new Code(
            400,
            "codes.resource.not.found"
    );

    public static final Code BAD_REQUEST_NEW_PASSWORD_INCORRECT = new Code(
            400,
            "codes.new-password.incorrect"
    );
}
