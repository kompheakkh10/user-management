package com.banking.user_management.service;

import com.banking.user_management.dto.request.EmailRequest;

public interface EmailService {

    void send(EmailRequest emailRequest);

}
