package com.banking.user_management.service;

import com.banking.user_management.dto.pagination.PaginationRequest;
import com.banking.user_management.dto.pagination.PaginationResponse;
import com.banking.user_management.dto.request.CreateRoleRequest;
import com.banking.user_management.dto.request.RevokeRolePermissionRequest;
import com.banking.user_management.dto.request.UpdateRolePermissionRequest;
import com.banking.user_management.dto.response.ApiDetailsResponseDto;
import com.banking.user_management.dto.response.ApiResponseDto;
import com.banking.user_management.dto.response.RoleFilterResponse;

public interface RoleService {
    ApiResponseDto createRole(CreateRoleRequest request);

    ApiResponseDto revokeRolePermission(RevokeRolePermissionRequest request);

    ApiResponseDto updateRolePermission(UpdateRolePermissionRequest request);

    ApiResponseDto deleteRole(Integer roleId);

    ApiDetailsResponseDto<PaginationResponse<RoleFilterResponse>> listRole(PaginationRequest request);
}
