package com.banking.user_management.service.imp;

import com.banking.user_management.Codes;
import com.banking.user_management.dto.pagination.PaginationRequest;
import com.banking.user_management.dto.pagination.PaginationResponse;
import com.banking.user_management.dto.request.CreateRoleRequest;
import com.banking.user_management.dto.request.RevokeRolePermissionRequest;
import com.banking.user_management.dto.request.UpdateRolePermissionRequest;
import com.banking.user_management.dto.response.ApiDetailsResponseDto;
import com.banking.user_management.dto.response.ApiResponseDto;
import com.banking.user_management.dto.response.RoleFilterResponse;
import com.banking.user_management.entity.Permission;
import com.banking.user_management.entity.Role;
import com.banking.user_management.entity.RolePermission;
import com.banking.user_management.exception.CustomNotFoundException;
import com.banking.user_management.exception.NotFoundException;
import com.banking.user_management.mapper.MapperUtils;
import com.banking.user_management.repository.PermissionRepository;
import com.banking.user_management.repository.RolePermissionRepository;
import com.banking.user_management.repository.RoleRepository;
import com.banking.user_management.service.RoleService;
import com.banking.user_management.specification.PaginationBaseHelper;
import com.banking.user_management.specification.RoleSpecification;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RoleServiceImp implements RoleService {

    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;
    private final RolePermissionRepository rolePermissionRepository;
    private final MessageSource messageSource;

    public RoleServiceImp(RoleRepository roleRepository, PermissionRepository permissionRepository, RolePermissionRepository rolePermissionRepository, @Qualifier("messageSource") MessageSource messageSource) {
        this.roleRepository = roleRepository;
        this.permissionRepository = permissionRepository;
        this.rolePermissionRepository = rolePermissionRepository;
        this.messageSource = messageSource;
    }

    @Override
    public ApiResponseDto createRole(CreateRoleRequest request) {
        log.info("Request permissions IDs: {}", request.getListPermissionName());
        final List<Permission> permissions = permissionRepository.findAllByName(request.getListPermissionName());

        log.info("permission size: {}", permissions.size());

        validationListPermissionRequestWithDb(request.getListPermissionName(), permissions);

        Role role = roleRepository.findByRoleName(request.getRoleName());

        if (role == null) {
            role = MapperUtils.MAPPER.mapToRole(request);
        }

        // save new role into database
        roleRepository.save(role);

        // when save in the db role object will automatically have the id from the db
        List<RolePermission> rolePermissionList = MapperUtils.MAPPER.mapToRolePermissions(role, permissions.stream().map(Permission::getName).toList());

        rolePermissionRepository.saveAll(rolePermissionList);

        return ApiResponseDto.create();
    }

    @Override
    public ApiResponseDto revokeRolePermission(RevokeRolePermissionRequest request) {
        final Role role = roleRepository.findById(request.getRoleId()).orElseThrow(CustomNotFoundException::new);

        List<RolePermission> rolePermissionList  = rolePermissionRepository.findAllByRoleIdAndPermissionNameIn(role.getId(), request.getListOfPermissions());
        log.info("Role Permission List: {}", rolePermissionList);

        if (rolePermissionList.size() != request.getListOfPermissions().size()) {
            throw new NotFoundException(Codes.RESOURCE_NOT_FOUND, messageSource);
        }

        rolePermissionRepository.deleteAll(rolePermissionList);

        return ApiResponseDto.success();
    }

    @Override
    public ApiResponseDto updateRolePermission(UpdateRolePermissionRequest request) {
        Role role = findRoleById(request.getRoleId());

        MapperUtils.MAPPER.mapToRole(role, request);

        roleRepository.save(role);

        return ApiResponseDto.success();
    }

    @Transactional
    @Override
    public ApiResponseDto deleteRole(Integer roleId) {
        final Role role = findRoleById(roleId);

        rolePermissionRepository.deleteAllByRoleId(role.getId());
        log.info("Successfully deleted role with id: {}", role.getId());

        roleRepository.delete(role);

        return ApiResponseDto.success();
    }

    @Override
    public ApiDetailsResponseDto<PaginationResponse<RoleFilterResponse>> listRole(PaginationRequest request) {
        final Pageable pageable = PaginationBaseHelper.createPageable(request.getPage(), request.getSize());
        final Specification<Role> specification = RoleSpecification.findAll();

        final Page<Role> rolePage = roleRepository.findAll(specification, pageable);

        // map to filter response
        final List<RoleFilterResponse> roleFilterResponses = rolePage.getContent()
                .stream()
                .map(MapperUtils.MAPPER::mapToRoleFilterResponse)
                .toList();


        final PaginationResponse<RoleFilterResponse> paginationResponse = new PaginationResponse<>(
                rolePage.getNumber(),
                rolePage.getTotalPages(),
                rolePage.getTotalElements(),
                rolePage.getSize(),
                roleFilterResponses
        );

        return new ApiDetailsResponseDto<>(
                "success",
                HttpStatus.OK.value(),
                paginationResponse
        );
    }

    private Role findRoleById(Integer roleId){
        return roleRepository.findById(roleId).orElseThrow(CustomNotFoundException::new);
    }

    private void validationListPermissionRequestWithDb(List<String> listPermissionRequest, List<Permission> listPermissionDb){
        if (listPermissionDb.size() != listPermissionRequest.size()){
            log.error("Some permission with the given id not found in the database!");
            throw new NotFoundException(Codes.RESOURCE_NOT_FOUND, messageSource);
        }
    }
}
