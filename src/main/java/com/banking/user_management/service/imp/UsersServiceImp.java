package com.banking.user_management.service.imp;

import com.banking.user_management.Codes;
import com.banking.user_management.config.JwtTokenProvider;
import com.banking.user_management.dto.RevokeUserPermission;
import com.banking.user_management.dto.UserUpdateRequest;
import com.banking.user_management.dto.pagination.PaginationResponse;
import com.banking.user_management.dto.request.*;
import com.banking.user_management.dto.response.*;
import com.banking.user_management.entity.*;
import com.banking.user_management.exception.*;
import com.banking.user_management.mapper.MapperUtils;
import com.banking.user_management.mapper.ResponseMapper;
import com.banking.user_management.mapper.UsersAuthenticationMapper;
import com.banking.user_management.repository.*;
import com.banking.user_management.service.EmailService;
import com.banking.user_management.service.UserAuthenticationService;
import com.banking.user_management.service.UsersService;
import com.banking.user_management.specification.PaginationBaseHelper;
import com.banking.user_management.specification.UserSpecification;
import com.banking.user_management.utils.PasswordGenerator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class UsersServiceImp implements UsersService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserAuthenticationService userAuthenticationService;
    private final UserAuthenticationRepository userAuthenticationRepository;
    private final EmailService emailService;
    private final PasswordChangeRepository passwordChangeRepository;
    private final RoleRepository roleRepository;
    private final UsersRoleRepository usersRoleRepository;
    private final MessageSource messageSource;

    @Override
    public ApiResponseDto createUser(CreateUserRequest request) throws Exception {
        log.info("CreateUser service...");

        Optional<Users> usersOptional = userRepository.findByUsername(request.getUsername());

        if (usersOptional.isPresent()) {
            log.error("User already registered with username: {}", request.getUsername());
            throw new CustomBadRequestException(Codes.BAD_REQUEST_USER_ALREADY_EXIST, messageSource, request.getUsername());
        }

        Users user = MapperUtils.MAPPER.userRequestToUsersEntity(request);

        // encode the password before insert into database
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        try {
            userRepository.save(user);
            log.info("New User has been created with username: {}", request.getUsername());

        } catch (Exception exception) {
            log.error("Error while attempting insert new user into database: {}", exception.getMessage());
            throw new ErrorDatabaseCustom(Codes.BAD_REQUEST_INTERNAL_SERVER_ERROR, messageSource);
        }

        return ApiResponseDto.create();
    }

    @Override
    public ApiDetailsResponseDto<?> login(LoginRequest request) {
        log.info("Login service...");

        Users user = userRepository.findByUsername(request.getUsername()).orElseThrow(
                () -> new  NotFoundException(Codes.NOT_FOUND_USER_BY_USERNAME,messageSource, request.getUsername())
        );

        UserAuthentication userAuth = userAuthenticationService.findUserAuthentication(user.getId());

        if (userAuth != null && (userAuth.getAttemptFail() >= 4 || user.isDeleted() || !user.isEnabled())) {
            lockUser(user);
            throw new CustomUnauthorizedCustom(Codes.BAD_REQUEST_USER_LOCKED, messageSource);
        }

        try {
            Authentication authentication = authenticateUser(request);
            return handleSuccessfulAuthentication(user, userAuth, authentication);
        } catch (Exception ex) {
            log.error("Authentication failed: {}", ex.getMessage());
            handleFailedAuthentication(user, userAuth);
            throw new CustomUnauthorizedCustom(Codes.BAD_REQUEST_INCORRECT_PASSWORD, messageSource);
        }
    }

    private void lockUser(Users user) {
        log.error("User attempt fail reached limit: {}", user.getUsername());
        user.setEnabled(false);
        userRepository.save(user);
    }

    private Authentication authenticateUser(LoginRequest request) {
        log.info("Authentication process...");
        return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
        );
    }

    private ApiDetailsResponseDto<?> handleSuccessfulAuthentication(Users user, UserAuthentication userAuth, Authentication authentication) {
        log.info("Authentication successful for user: {}", user.getUsername());

        final String accessToken = jwtTokenProvider.generateToken(authentication);
        final String refreshToken = jwtTokenProvider.generateRefreshToken(user.getUsername());

        if (userAuth != null) {
            updateUserAuthentication(userAuth, accessToken, refreshToken, 0);
        } else {
            saveNewUserAuthentication(user, accessToken, refreshToken, 0);
        }

        log.info("User authentication updated for user: {}", user.getUsername());
        return ResponseMapper.MAPPER.mapToLoginResponses(accessToken, user.getUsername(), refreshToken);
    }

    private void handleFailedAuthentication(Users user, UserAuthentication userAuth) {
        int attempts = 1;

        if (userAuth != null) {
            int updatedAttempts = userAuth.getAttemptFail() + attempts;
            updateUserAuthentication(userAuth, null, null, updatedAttempts);
        } else {
            saveNewUserAuthentication(user, null, null, attempts);
        }
    }

    private void updateUserAuthentication(UserAuthentication userAuth, String accessToken, String refreshToken, int attempts) {
        userAuth.setAccessToken(accessToken);
        userAuth.setRefreshToken(refreshToken);
        userAuth.setAttemptFail(attempts);
        userAuthenticationRepository.save(userAuth);
    }

    private void saveNewUserAuthentication(Users user, String accessToken, String refreshToken, int attempts) {
        UserAuthentication newUserAuth = UsersAuthenticationMapper.MAPPER.mapToUserAuthentication(refreshToken, accessToken, user, attempts);
        userAuthenticationRepository.save(newUserAuth);
    }

    @Override
    public ApiResponseDto enabledUser(EnableUserRequest request) {
        log.info("EnableUser service...");

        Users user = findUserByUsername(request.getUsername());

        UserAuthentication userAuthentication = userAuthenticationService.findUserAuthentication(user.getId());

        if (!Objects.isNull(userAuthentication)) {

            // change status user
            user.setEnabled(Boolean.TRUE);
            userRepository.save(user);

            // change attempt fail to zero
            userAuthentication.setAttemptFail(0);
            userAuthenticationRepository.save(userAuthentication);
        } else {
            throw new NotFoundException(Codes.RESOURCE_NOT_FOUND, messageSource);
        }

        return ApiResponseDto.success();
    }

    @Override
    public ApiResponseDto disableUser(DisableUser request) {
        Users user = findUserByUsername(request.getUsername());

        user.setEnabled(Boolean.FALSE);
        userRepository.save(user);

        return ApiResponseDto.success();
    }

    @Override
    public ApiResponseDto deleteUser(DeleteUserRequest request) {
        Users user = findUserByUsername(request.getUsername());

        user.setDeleted(Boolean.TRUE);
        userRepository.save(user);

        return ApiResponseDto.success();
    }

    @Override
    public ApiResponseDto updateUser(UserUpdateRequest request, Integer userId) {
        Users user = userRepository.findById(userId).orElseThrow(
                () -> {
                    log.error("User not found with this id: {}", userId);
                    return new NotFoundException(Codes.RESOURCE_NOT_FOUND, messageSource);
                }
        );

        MapperUtils.MAPPER.userUpdateRequestToUserEntity(user, request);

        // save the updated object to the database
        userRepository.save(user);
        return ApiResponseDto.success();
    }

    @Override
    public ApiResponseDto resetPassword(ResetPassword request, Integer userId) {

        Users user = findUserById(userId);
        final boolean isMatchPassword = passwordEncoder.matches(request.getCurrentPassword(), user.getPassword());

        if (!isMatchPassword) {
            log.error("The current password is incorrect!");
            throw new CustomBadRequestException(Codes.BAD_REQUEST_INCORRECT_PASSWORD, messageSource);
        }

        final String randomPassword = PasswordGenerator.generateRandomPassword();

        emailService.send(MapperUtils.MAPPER.mapToEmailRequest(user, randomPassword));


        PasswordChange passwordChangeDb = passwordChangeRepository.findByUserId(user.getId());

        // user request change password first time
        // variable for seven day expiration
        long sevenDaysInMillis = 7L * 24 * 60 * 60 * 1000;
        if (Objects.isNull(passwordChangeDb)) {
            PasswordChange passwordChange = MapperUtils.MAPPER.mapToPasswordChange(user, randomPassword, new Timestamp(System.currentTimeMillis() + sevenDaysInMillis));
            passwordChangeRepository.save(passwordChange);
        }

        MapperUtils.MAPPER.mapToPasswordChangeExist(passwordChangeDb, user, randomPassword, new Timestamp(System.currentTimeMillis() + sevenDaysInMillis));
        passwordChangeRepository.save(passwordChangeDb);

        return ApiResponseDto.success();
    }

    @Override
    public ApiResponseDto changeNewPassword(ChangeNewPasswordRequest request, @AuthenticationPrincipal UserDetails userDetails) {

        PasswordChange passwordChange = passwordChangeRepository.findByPasswordKey(request.getPasswordKey());
        if (Objects.isNull(passwordChange)) {
            log.error("Password key not found with key: {}", request.getPasswordKey());
            throw new CustomBadRequestException(Codes.BAD_REQUEST, messageSource);
        }

        if (passwordChange.getPasswordKey().equals(request.getPasswordKey()) &&
                passwordChange.getUser().getUsername().equals(userDetails.getUsername()) &&
                passwordChange.getExpiredDate().after(new Timestamp(System.currentTimeMillis()))) {

            if (!request.getNewPassword().equals(request.getConfirmPassword())) {
                log.error("New password does not match confirm password!");
                throw new CustomBadRequestException(Codes.BAD_REQUEST_NEW_PASSWORD_INCORRECT, messageSource);
            }

            Users user = findUserByUsername(userDetails.getUsername());

            final String newPasswordEncoded = passwordEncoder.encode(request.getNewPassword());
            user.setPassword(newPasswordEncoded);

            userRepository.save(user);

            // delete the keyPassword.
            passwordChangeRepository.delete(passwordChange);
        } else {
            log.error("Password key and password do not match!");
            throw new CustomBadRequestException(Codes.BAD_REQUEST_PASSWORD_KEY_NOT_MATCH, messageSource);
        }

        return ApiResponseDto.success();
    }

    @Override
    public ApiResponseDto assignRoleToUser(AssignRoleRequest request) {
        Users user = findUserById(request.getUserId());

        List<Role> listRole = findRolesByIds(request.getListRoleId());
        log.info("Role list : {}", listRole);

        listRole.forEach(r -> {
            final UsersRoles usersRoles = MapperUtils.MAPPER.mapToRolePermission(user, r);
            usersRoleRepository.save(usersRoles);
        });

        return ApiResponseDto.success();
    }

    @Override
    public ApiDetailsResponseDto<PaginationResponse<UserResponseFilter>> listUser(ListUserRequest request) {

        final Pageable pageable = PaginationBaseHelper.createPageable(request.getPage(), request.getSize());
        final Specification<Users> specification = UserSpecification.findAllUserLikeUsername(request.getUsername());

        final Page<Users> users = userRepository.findAll(specification, pageable);

        final List<UserResponseFilter> filterResponseList = users.getContent()
                .stream()
                .map(MapperUtils.MAPPER::mapToUsersResponseFilter)
                .toList();

        final PaginationResponse<UserResponseFilter> paginationResponse = new PaginationResponse<>(
                users.getNumber(),
                users.getTotalPages(),
                users.getTotalElements(),
                users.getSize(),
                filterResponseList
        );

        return new ApiDetailsResponseDto<>(
                "success",
                HttpStatus.OK.value(),
                paginationResponse
        );
    }

    @Override
    public ApiDetailsResponseDto<UserProfileResponse> viewProfile(Integer id) {
        Users user = findUserById(id);

        final UserProfileResponse userProfileResponse = MapperUtils.MAPPER.mapToUserProfileResponse(user);

        return new ApiDetailsResponseDto<>(
                "success",
                HttpStatus.OK.value(),
                userProfileResponse
        );
    }

    @Override
    public ApiDetailsResponseDto<?> getAccessToken(GetAccessTokenRequest request, @AuthenticationPrincipal UserDetails userDetails) {
        Users user = findUserByUsername(userDetails.getUsername());

        UserAuthentication userAuthentication = userAuthenticationRepository.findByUserId(user.getId()).orElseThrow(
                () -> new CustomBadRequestException(Codes.RESOURCE_NOT_FOUND, messageSource)
        );

        if (!Objects.equals(request.getRefreshToken(), userAuthentication.getRefreshToken())){
            log.error("Invalid refresh token!");
            throw new CustomBadRequestException(Codes.BAD_REQUEST, messageSource);
        }

        if (Boolean.FALSE.equals(jwtTokenProvider.validateToken(request.getRefreshToken(), userDetails))){
            log.error("Invalid refresh token or expired refresh token!");
            throw new CustomBadRequestException(Codes.BAD_REQUEST, messageSource);
        }

        String accessToken = jwtTokenProvider.generateToken(userDetails);

        userAuthentication.setAccessToken(accessToken);
        userAuthenticationRepository.save(userAuthentication);

        return ResponseMapper.MAPPER.mapToLoginResponses(accessToken, user.getUsername(), userAuthentication.getRefreshToken());
    }

    private Users findUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(
                () -> {
                    log.error("User not found with username: {}", username);
                    return new NotFoundException(Codes.NOT_FOUND_USER_BY_USERNAME, messageSource);
                }
        );
    }

    private List<Role> findRolesByIds(List<Integer> listRoleId) {
        // Find all roles by their IDs
        List<Role> listRole = roleRepository.findAllById(listRoleId);

        // Check if any role is missing
        if (listRole.size() != listRoleId.size()) {
            throw new NotFoundException(Codes.RESOURCE_NOT_FOUND, messageSource);
        }

        // Return the list of roles
        return listRole;
    }


    private Users findUserById(Integer id) {
        return userRepository.findById(id).orElseThrow(
                () -> {
                    log.error("User not found with id: {}", id);
                    return new NotFoundException(Codes.RESOURCE_NOT_FOUND, messageSource);
                }
        );
    }

    @Override
    public ApiResponseDto revokeUserPermission(RevokeUserPermission revokeUserPermission) {
        return null;
    }
}
