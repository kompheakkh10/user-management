package com.banking.user_management.service.imp;

import com.banking.user_management.dto.pagination.PaginationRequest;
import com.banking.user_management.dto.pagination.PaginationResponse;
import com.banking.user_management.dto.response.ApiDetailsResponseDto;
import com.banking.user_management.dto.response.PermissionResponseFilter;
import com.banking.user_management.entity.Permission;
import com.banking.user_management.mapper.MapperUtils;
import com.banking.user_management.repository.PermissionRepository;
import com.banking.user_management.service.PermissionService;
import com.banking.user_management.specification.PaginationBaseHelper;
import com.banking.user_management.specification.PermissionSpecification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PermissionServiceImp implements PermissionService {

    private final PermissionRepository permissionRepository;

    public PermissionServiceImp(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    @Override
    public ApiDetailsResponseDto<PaginationResponse<PermissionResponseFilter>> getPermissionsWithPagination(PaginationRequest request) {
        final Pageable pageable = PaginationBaseHelper.createPageable(request.getPage(), request.getSize());
        final Specification<Permission> specification = PermissionSpecification.findAll();
        final Page<Permission> permissionPage = permissionRepository.findAll(specification, pageable);

        final List<PermissionResponseFilter> permissionResponseFilters = permissionPage.getContent()
                .stream()
                .map(MapperUtils.MAPPER::mapToResponseFilter)
                .collect(Collectors.toList());

        final PaginationResponse<PermissionResponseFilter> paginationResponse = new PaginationResponse<>(
                permissionPage.getNumber(),
                permissionPage.getTotalPages(),
                permissionPage.getTotalElements(),
                permissionPage.getSize(),
                permissionResponseFilters
        );

        return new ApiDetailsResponseDto<>(
                "success",
                HttpStatus.OK.value(),
                paginationResponse
        );
    }
}
