package com.banking.user_management.service.imp;

import com.banking.user_management.entity.UserAuthentication;
import com.banking.user_management.repository.UserAuthenticationRepository;
import com.banking.user_management.service.UserAuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class UserAuthenticationServiceImp implements UserAuthenticationService {

    private final UserAuthenticationRepository userAuthenticationRepository;

    public UserAuthenticationServiceImp(UserAuthenticationRepository userAuthenticationRepository) {
        this.userAuthenticationRepository = userAuthenticationRepository;
    }

    @Override
    public UserAuthentication findUserAuthentication(Integer userId){
        Optional<UserAuthentication> userAuthenticationOptional = userAuthenticationRepository.findByUserId(userId);

        if (userAuthenticationOptional.isEmpty()){
            log.error("User not found with this userId: {}", userId);
            return null;
        }

        return userAuthenticationOptional.get();
    }

}
