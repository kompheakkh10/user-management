package com.banking.user_management.service.imp;

import com.banking.user_management.dto.request.EmailRequest;
import com.banking.user_management.service.EmailService;
import jakarta.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

@Service
@Slf4j
public class EmailServiceImp implements EmailService {

    private final JavaMailSender mailSender;

    @Value("${email.sender}")
    private String emailSender;

    private final SpringTemplateEngine templateEngine;

    public EmailServiceImp( JavaMailSender mailSender, SpringTemplateEngine templateEngine){
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    @Override
    public void send(EmailRequest emailRequest) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);

            helper.setFrom(emailSender);
            helper.setTo(emailRequest.getSendTo());
            helper.setSubject("Password Request Change");

            // Create the email body using Thymeleaf
            Context context = new Context();
            context.setVariable("username", emailRequest.getUsername());
            context.setVariable("randomPassword", emailRequest.getRandomPassword());

            final String emailBody = templateEngine.process("password-change", context);
            helper.setText(emailBody, true);

            mailSender.send(message);
            log.info("Password change email sent successfully....");
        } catch (Exception exception) {
            log.error("Error sending email: {}", exception.getMessage());
        }
    }

}
