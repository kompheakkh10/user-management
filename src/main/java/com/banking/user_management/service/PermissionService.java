package com.banking.user_management.service;

import com.banking.user_management.dto.pagination.PaginationRequest;
import com.banking.user_management.dto.pagination.PaginationResponse;
import com.banking.user_management.dto.response.ApiDetailsResponseDto;
import com.banking.user_management.dto.response.PermissionResponseFilter;

public interface PermissionService {

    ApiDetailsResponseDto<PaginationResponse<PermissionResponseFilter>> getPermissionsWithPagination(PaginationRequest request);

}
