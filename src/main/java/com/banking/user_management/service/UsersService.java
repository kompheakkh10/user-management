package com.banking.user_management.service;

import com.banking.user_management.dto.RevokeUserPermission;
import com.banking.user_management.dto.UserUpdateRequest;
import com.banking.user_management.dto.pagination.PaginationResponse;
import com.banking.user_management.dto.request.*;
import com.banking.user_management.dto.response.ApiDetailsResponseDto;
import com.banking.user_management.dto.response.ApiResponseDto;
import com.banking.user_management.dto.response.UserProfileResponse;
import com.banking.user_management.dto.response.UserResponseFilter;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;

public interface UsersService {

    ApiResponseDto createUser(CreateUserRequest request) throws Exception;
    ApiDetailsResponseDto<?> login(LoginRequest request);
    ApiResponseDto enabledUser(EnableUserRequest request);
    ApiResponseDto disableUser(DisableUser request);
    ApiResponseDto deleteUser(DeleteUserRequest request);
    ApiResponseDto updateUser(UserUpdateRequest request, Integer userId);
    ApiResponseDto resetPassword(ResetPassword request, Integer userId);
    ApiResponseDto changeNewPassword(ChangeNewPasswordRequest request, @AuthenticationPrincipal UserDetails userDetails);
    ApiResponseDto assignRoleToUser(AssignRoleRequest request);
    ApiDetailsResponseDto<PaginationResponse<UserResponseFilter>> listUser(ListUserRequest request);
    ApiDetailsResponseDto<UserProfileResponse> viewProfile(Integer id);
    ApiDetailsResponseDto<?> getAccessToken(GetAccessTokenRequest request, @AuthenticationPrincipal UserDetails userDetails);
    ApiResponseDto revokeUserPermission(RevokeUserPermission revokeUserPermission);
}
