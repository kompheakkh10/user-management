package com.banking.user_management.service;

import com.banking.user_management.entity.UserAuthentication;

public interface UserAuthenticationService {

    UserAuthentication findUserAuthentication(Integer userId);

}
