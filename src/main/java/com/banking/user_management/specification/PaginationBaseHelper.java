package com.banking.user_management.specification;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class PaginationBaseHelper {
    private PaginationBaseHelper(){}

    private static final int DEFAULT_PAGE = 0;
    private static final int DEFAULT_SIZE = 10;

    public static Pageable createPageable(Integer page, Integer size) {
        int pageNumber = (page != null && page >= 0) ? page : DEFAULT_PAGE;
        int pageSize = (size != null && size > 0) ? size : DEFAULT_SIZE;

        return PageRequest.of(pageNumber, pageSize);
    }

}
