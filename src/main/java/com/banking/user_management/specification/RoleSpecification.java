package com.banking.user_management.specification;

import com.banking.user_management.entity.Role;
import org.springframework.data.jpa.domain.Specification;

public class RoleSpecification {

    private RoleSpecification() {}

    public static Specification<Role> findAll(){
        return (root, query, criteriaBuilder) -> {
            query.orderBy(criteriaBuilder.asc(root.get("roleName")));
            return criteriaBuilder.conjunction();
        };
    }

}
