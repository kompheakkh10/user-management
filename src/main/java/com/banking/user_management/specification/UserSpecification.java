package com.banking.user_management.specification;

import com.banking.user_management.entity.Users;
import org.springframework.data.jpa.domain.Specification;

public class UserSpecification {

    public static Specification<Users> findAllUserLikeUsername(String username) {
        return (root, query, criteriaBuilder) -> {
            if (username == null || username.isEmpty()) {
                return null; // No filtering if username is null or empty
            }

            String pattern = "%" + username.toLowerCase() + "%"; // Case-insensitive search

            return criteriaBuilder.like(criteriaBuilder.lower(root.get("username")), pattern);
        };

    }
}
