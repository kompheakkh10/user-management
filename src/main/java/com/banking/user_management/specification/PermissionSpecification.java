package com.banking.user_management.specification;

import com.banking.user_management.entity.Permission;
import org.springframework.data.jpa.domain.Specification;

public class PermissionSpecification {

    private PermissionSpecification() {}

    public static Specification<Permission> findAll() {
        return (root, query, criteriaBuilder) -> {
            query.orderBy(criteriaBuilder.asc(root.get("name")));
            return criteriaBuilder.conjunction();
        };
    }

}
