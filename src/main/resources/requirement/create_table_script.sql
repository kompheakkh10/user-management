    create table if not exists public.users
    (
        id            serial
        primary key,
        username      varchar(255) not null,
        password      varchar(255) not null,
        email         varchar(255),
        is_enabled    boolean      not null,
        description   varchar(255),
        is_deleted    boolean      not null,
        login_attempt integer
        );

    alter table public.users
        owner to admin;

    create table if not exists public.permissions
    (
        name         varchar(255) not null
        unique,
        created_date timestamp(0) not null,
        updated_date timestamp(0),
        remarks      varchar(255)
        );

    alter table public.permissions
        owner to admin;

    create table if not exists public.roles
    (
        id          serial
        primary key,
        role_name   varchar(255) not null,
        description varchar(255)
        );

    alter table public.roles
        owner to admin;

    create table if not exists public.users_roles
    (
        user_id integer not null
        constraint users_roles_user_id_foreign
        references public.users,
        role_id integer not null
        constraint users_roles_role_id_foreign
        references public.roles
    );

    alter table public.users_roles
        owner to admin;

    create table if not exists public.roles_permission
    (
        role_id         integer      not null
        constraint roles_permission_role_id_foreign
        references public.roles,
        permission_name varchar(255) not null
        constraint roles_permission_permission_name_foreign
        references public.permissions (name)
        );

    alter table public.roles_permission
        owner to admin;

    create table if not exists public.flyway_schema_history
    (
        installed_rank integer                 not null
        constraint flyway_schema_history_pk
        primary key,
        version        varchar(50),
        description    varchar(200)            not null,
        type           varchar(20)             not null,
        script         varchar(1000)           not null,
        checksum       integer,
        installed_by   varchar(100)            not null,
        installed_on   timestamp default now() not null,
        execution_time integer                 not null,
        success        boolean                 not null
        );

    alter table public.flyway_schema_history
        owner to admin;

    create index if not exists flyway_schema_history_s_idx
        on public.flyway_schema_history (success);

    create table if not exists public.users_authentication
    (
        user_id       integer
        references public.users,
        access_token  varchar(255),
        refresh_token varchar(255),
        attempt_fail  integer,
        id            serial
        );

    alter table public.users_authentication
        owner to admin;

    create table if not exists public.password_change
    (
        id           serial
        primary key,
        user_id      integer
        references public.users,
        password_key varchar,
        expired_date timestamp(0)
        );

    alter table public.password_change
        owner to admin;

